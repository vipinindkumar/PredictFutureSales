# PredictFutureSales

Predict total sales for every product and store in the next month from a  time-series dataset consisting of daily sales data

- sales1(oct_as_pred) notebook
    - using last month data (October) as predictions for next month data (November)
    - Score, root mean squared error (RMSE) => 1.16777
    

- sales modified data csv notebook
    - Create a grid of shop_ids and item_ids for different date_block_num with corresponding item_cnt_month
    

- sales modified data csv(mean enc) notebook
    - Add target mean encoding feature to the data
